package app

import (
	"gitlab.com/jayonq/golang-microservices/mvc/controllers"
)

func mapUrls() {
	router.GET("/users/:user_id", controllers.GetUser)

}
