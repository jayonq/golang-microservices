package main

import (
	"gitlab.com/jayonq/golang-microservices/mvc/app"
)

func main() {
	app.StartApp()
}
